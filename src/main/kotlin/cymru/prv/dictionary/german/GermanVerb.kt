package cymru.prv.dictionary.german

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import cymru.prv.dictionary.german.tenses.PresentTense
import cymru.prv.dictionary.german.tenses.PreteriteTense
import org.json.JSONObject

class GermanVerb(obj: JSONObject): Word(obj, WordType.verb) {

    val stem = normalForm.replace("en$".toRegex(), "")
    val tenses = mapOf(
        "present" to PresentTense(obj.optJSONObject("present")?: JSONObject(), this),
        "preterite" to PreteriteTense(obj.optJSONObject("preterite")?:JSONObject(), this)
    )

    val future = obj.optString("future", normalForm)

    override fun getConjugations(): JSONObject {
        val obj = JSONObject()
        obj.put("future", future)
        tenses.forEach { (name, tense) ->
            obj.put(name, tense.toJson())
        }
        return obj
    }

}