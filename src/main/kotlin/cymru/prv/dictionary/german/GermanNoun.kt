package cymru.prv.dictionary.german

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import org.json.JSONObject

class GermanNoun(obj: JSONObject): Word(obj, WordType.noun)