package cymru.prv.dictionary.german.tenses

import cymru.prv.dictionary.german.GermanVerb
import org.json.JSONObject

class PresentTense(obj: JSONObject, verb: GermanVerb): GermanVerbTense(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return mutableListOf(stem + "e")
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return mutableListOf(
            when {
                stem.endsWith("ss") -> stem + "t"
                else -> stem + "st"
            }
        )
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return mutableListOf(stem + "t")
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return mutableListOf(stem + "en")
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return mutableListOf(stem + "t")
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return mutableListOf(stem + "en")
    }

}