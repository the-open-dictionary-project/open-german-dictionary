package cymru.prv.dictionary.german.tenses

import cymru.prv.dictionary.common.Conjugation
import cymru.prv.dictionary.german.GermanVerb
import org.json.JSONObject

abstract class GermanVerbTense(obj: JSONObject, verb: GermanVerb): Conjugation(obj) {

    val stem = obj.optString("stem", verb.stem)

}

