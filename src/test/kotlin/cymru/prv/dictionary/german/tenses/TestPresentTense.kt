package cymru.prv.dictionary.german.tenses

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.german.GermanVerb
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestPresentTense {

    @Test
    fun `default suffixes should work`(){
        val verb = GermanVerb(JSONObject().put("normalForm", "brauchen"))
        val tense = PresentTense(JSONObject(), verb)
        assertEquals("brauche", tense.toJson().getJSONArray("singFirst").get(0))
        assertEquals("brauchst", tense.toJson().getJSONArray("singSecond").get(0))
        assertEquals("braucht", tense.toJson().getJSONArray("singThird").get(0))
        assertEquals("brauchen", tense.toJson().getJSONArray("plurFirst").get(0))
        assertEquals("braucht", tense.toJson().getJSONArray("plurSecond").get(0))
        assertEquals("brauchen", tense.toJson().getJSONArray("plurThird").get(0))
    }

    @Test
    fun `stems ending with double s should not receive an additional s for singular second`(){
        val verb = GermanVerb(JSONObject().put("normalForm", "passen"))
        val tense = PresentTense(JSONObject(), verb)
        assertEquals("passt", tense.toJson().getJSONArray("singSecond").get(0))
    }

}