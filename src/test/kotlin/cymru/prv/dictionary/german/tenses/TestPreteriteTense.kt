package cymru.prv.dictionary.german.tenses

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.german.GermanVerb
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestPreteriteTense {

    @Test
    fun `default suffixes should work`(){
        val verb = GermanVerb(JSONObject().put("normalForm", "brauchen"))
        val tense = PreteriteTense(JSONObject(), verb)
        assertEquals("brauchte", tense.toJson().getJSONArray("singFirst").get(0))
        assertEquals("brauchtest", tense.toJson().getJSONArray("singSecond").get(0))
        assertEquals("brauchte", tense.toJson().getJSONArray("singThird").get(0))
        assertEquals("brauchten", tense.toJson().getJSONArray("plurFirst").get(0))
        assertEquals("brauchtet", tense.toJson().getJSONArray("plurSecond").get(0))
        assertEquals("brauchten", tense.toJson().getJSONArray("plurThird").get(0))
    }

}